# PocketSphinx Android (Brazilian Portuguese version)

This is a demonstration for Pocketsphinx on Android. 

See for details http://cmusphinx.sourceforge.net/wiki/tutorialandroid

# :fox_face: Update from FalaBrasil :fox_face:
This repo was originally 
"[forked](https://stackoverflow.com/questions/50973048/forking-git-repository-from-github-to-gitlab)" 
from PocketSphinx's official GitHub repo 
https://github.com/cmusphinx/pocketsphinx-android-demo and is expected to
contain enough, detailed information on how to make an Android-based offline
speech recogniser in Brazilian Portuguese using PocketSphinx.

Apparently, CMU Sphinx's  up to date tutorial is available at
https://cmusphinx.github.io/wiki/tutorialandroid/. You might want to check that
out for more information.

## Steps for installation
- Download [Android Studio](https://developer.android.com/studio)
- Download [NDK and Tools](https://developer.android.com/ndk/guides)
    - **Tools** > **Android** > **SDK Manager**
    - Tab: "*SDK Tools*"
    - Check `LLDB`, `CMake` and `NDK`, then click "*Apply*" and "*Ok*"

## Tests: Hardware and Software specs (Aug 2019)
![August 2019](./doc/about_studio.png)
![August 2019](./doc/about_hardware.png)


[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2019)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/
